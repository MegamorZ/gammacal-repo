function poly1(En,Area,errY,tm)
%xdata = -5:5;
%ydata = xdata.^2 - 5*xdata - 3 + 1.5*randn(size(xdata));
close all

% En=[121.781700000000;244.697400000000;344.278500000000;411.116500000000;443.965000000000;778.904500000000;867.380000000000;964.079000000000;1085.83700000000;1112.07600000000;1408.01300000000];
% Area=[305929;65809;149542;10439;13547;30525;9014;27371;16974;22805;27344];
% %errY=sqrt(ydata);
int=[0.284100000000000;0.0755000000000000;0.265900000000000;0.0223800000000000;0.0280000000000000;0.129700000000000;0.0424300000000000;0.145000000000000;0.101300000000000;0.134100000000000;0.208500000000000];
D=88849;
M=1;
tm=8146.62;

Eff=(Area./(tm*int))/(M*D);

xdata=log(En);
ydata=log(Eff);
errY=sqrt(ydata)/10;

degree = 3;		% Degree of the fit
alpha = 0.05;	% Significance level

% Compute the fit and return the structure used by 
% POLYCONF.
[p,S] = polyfit(xdata,ydata,degree);



%% Extent of the data.
mx = min(xdata);
Mx = max(xdata);
my = min(ydata);
My = max(ydata);

% Scale factors for plotting.
sx = 0.05*(Mx-mx);
sy = 0.05*(My-my);

%% Plots
subplot(4,1,[1 3]),plot(xdata,ydata,'rd','MarkerSize',5,...
		'LineWidth',2);
hold on
%errorbar(xdata,ydata,errY,'.')
xfit = mx-sx:0.01:Mx+sx;
yfit = polyval(p,xfit);
hfit = plot(xfit,yfit,'b-','LineWidth',2);

grid on

%plot(xfit,zeros(size(xfit)),'k-','LineWidth',2)%linea negra en cero
axis([mx-sx Mx+sx my-sy My+sy])

% Add prediction intervals to the plot.
[Y,DELTA] = polyconf(p,xfit,S,'alpha',alpha);
% hconf = plot(xfit,Y+DELTA,'b--');
% plot(xfit,Y-DELTA,'b--')
fill([xfit fliplr(xfit)],[Y+DELTA fliplr(Y-DELTA)],1,'facecolor','red','edgecolor','none','facealpha',0.4)

%xlabel('ln (Energy)');
ylabel('ln (Efficiency)');

%Plot residues
y = polyval(p,xdata);
Res=ydata-y;
subplot(4,1,4),bar(xdata,Res,4,'r')%'FaceColor',[0 .5 .5],'EdgeColor',[0 .9 .9],'LineWidth',1.5)
hold on
plot(xfit,zeros(size(xfit)),'k-','LineWidth',2)%linea negra en cero
axis([mx-sx Mx+sx min(Res)+0.1*min(Res) max(Res)+0.1*max(Res)])

xlabel('ln (Energy)');
ylabel('Residue');

eqn = ['y = ' sprintf('%3.3fx^%1.0f + ',[P ;length(P)-1:-1:0])];
eqn = eqn(1:end-3)
end
