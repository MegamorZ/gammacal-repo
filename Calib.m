function Calib(En,Area,errY,tm)

int=[0.284100000000000;0.0755000000000000;0.265900000000000;0.0223800000000000;0.0280000000000000;0.129700000000000;0.0424300000000000;0.145000000000000;0.101300000000000;0.134100000000000;0.208500000000000];

D=88849;
M=1;
%tm=8146.62;

Eff=(Area./(tm*int))/(M*D);

xdata=log(En);
ydata=log(Eff)
errY=log(errY.*Eff./Area)/100

degree = 3;		% Degree of the fit
alpha = 0.05;	% Significance level

% Compute the fit and return the structure used by 
% POLYCONF.
[p,S] = polyfit(xdata,ydata,degree);



%% Extent of the data.
mx = min(xdata);
Mx = max(xdata);
my = min(ydata);
My = max(ydata);

% Scale factors for plotting.
sx = 0.05*(Mx-mx);
sy = 0.05*(My-my);

%% Plots
%subplot(4,1,[1 3]),plot(xdata,ydata,'rd','MarkerSize',5,...
%		'LineWidth',2);

plot(app.G1, xdata,ydata)%,'rd','MarkerSize',5,'LineWidth',2);
hold(app.G1, 'on')
errorbar(xdata,ydata,errY,'.')
xfit = mx-sx:0.01:Mx+sx;
yfit = polyval(p,xfit);
hfit = plot(xfit,yfit,'b-','LineWidth',2);

grid on

%plot(xfit,zeros(size(xfit)),'k-','LineWidth',2)%linea negra en cero
axis([mx-sx Mx+sx my-sy My+sy])

% Add prediction intervals to the plot.
[Y,DELTA] = polyconf(p,xfit,S,'alpha',alpha);
% hconf = plot(xfit,Y+DELTA,'b--');
% plot(xfit,Y-DELTA,'b--')
fill([xfit fliplr(xfit)],[Y+DELTA fliplr(Y-DELTA)],1,'facecolor','red','edgecolor','none','facealpha',0.4)

%xlabel('ln (Energy)');
ylabel('ln (Efficiency)');

%Plot residues
y = polyval(p,xdata);
Res=ydata-y;
%subplot(4,1,4),bar(xdata,Res,2,'r')%'FaceColor',[0 .5 .5],'EdgeColor',[0 .9 .9],'LineWidth',1.5)
bar(app.Residue2,xdata,Res,2,'r')
hold(app.Residue, 'on')
plot(xfit,zeros(size(xfit)),'k-','LineWidth',2)%linea negra en cero
axis([mx-sx Mx+sx min(Res)+0.1*min(Res) max(Res)+0.1*max(Res)])

xlabel('ln (Energy)');
ylabel('Residue');
end
