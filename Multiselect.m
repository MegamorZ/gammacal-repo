%% Multiselect
[filenames, pathname] = uigetfile('MultiSelect', 'on');
filenames = cellstr(filenames);  % EDITED
for n = 1:length(filenames)
  afile = fullfile(pathname, filenames{n});  % EDITED
  data(:,n) = importfile1(afile);
  % Remove the file extension file the name:
%   [dummy, afilename] = fileparts(filenames{n});
%   save(afilename, 'data');
end

[dummy, afilename] = fileparts(filenames{n});
save(afilename, 'data');